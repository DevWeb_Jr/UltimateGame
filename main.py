from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from Game.Connection import ConnectionController
from Game.Map import MapController


class Window(GridLayout):
    """this class implements a method for clear all widgets before displaying a view"""
    def __init__(self, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)

    def display_screen(self, screen):
        self.clear_widgets()
        self.add_widget(screen)


class Screens():
    """this class is a manager for all views"""
    def __init__(self):
        self.window = Window()
        self.connection = ConnectionController(self)
        self.map = MapController(self)

        self.window.display_screen(self.connection.view)

    def display(self, screen):
        self.window.display_screen(screen.view)


class Game(App):
    def build(self):
        self.title = "Game | Kivy App"
        self.icon = './Images/link.png'
        screen = Screens()
        return screen.window


if __name__ == '__main__':
    Game().run()
