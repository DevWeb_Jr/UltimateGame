from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button


class StandardButton(Button):
    def __init__(self, text="press", size_hint=(.20, .20),
                 center_x=.5, center_y=.5, **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.font_size = 18
        self.bold = True
        self.color = "#82ccdd"
        self.size_hint = size_hint
        self.center_x = center_x
        self.center_y = center_y
        self.background_color = "#0a3d62"
        self.background_normal = ""
        self.pos_hint = {"center_x": .5, "center_y": .5}


class ReturnButton(Button):
    def __init__(self, text="return", **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.font_size = 9
        self.bold = True
        self.color = "#82ccdd"
        self.background_color = "#0a3d62"
        self.background_normal = ""
        self.size_hint = (None, None)
        self.pos =(0, 690)
        self.size = (50, 30)


class QuitButton(Button):
    def __init__(self, text="X", **kwargs):
        Button.__init__(self, **kwargs)
        self.text = text
        self.bold = True
        self.color = "#ffffff"
        self.background_color = "#eb2f06"
        self.background_normal = ""
        self.size_hint = (None, None)
        self.pos =(1250, 690)
        self.size = (30, 30)


class Touch(BoxLayout):
    def __init__(self, **kwargs):
        BoxLayout.__init__(self, **kwargs)
        self.button = Button(text="-")
        self.button.pos_hint = {"center_x": .5, "center_y": .5}
        self.button.bold = True
        self.button.color = "#82ccdd"
        self.button.background_color = "#0a3d62"
        self.button.background_normal = ""

        self.add_widget(self.button)
