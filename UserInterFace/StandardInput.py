from kivy.uix.textinput import TextInput


class StandardInput(TextInput):
    def __init__(self, text="", **kwargs):
        TextInput.__init__(self, **kwargs)
        self.text = text
        self.multiline = False
        self.font_size = 80
        self.foreground_color = "#4a69bd"
        self.cursor_color = "#4a69bd"
        self.background_color = "#6a89cc"
        self.halign = "right"
        self.padding_y = (5, 5)
        self.size_hint = (.3, 0.3)
        
