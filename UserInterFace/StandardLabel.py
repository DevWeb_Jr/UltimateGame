from kivy.uix.label import Label


class StandardLabel(Label):
    def __init__(self, text="text"):
        Label.__init__(self)
        self.text = text
        self.bold = True
        self.background_color = "#6a89cc"
