from kivy.uix.image import Image
from kivy.uix.gridlayout import GridLayout
from kivy.uix.anchorlayout import AnchorLayout
from UserInterFace.StandardButton import Touch


class Joystick(GridLayout):
    def __init__(self, character):
        GridLayout.__init__(self)
        self.cols = 3
        self.rows = 3
        self.character = character

        self.a = AnchorLayout()
        self.add_widget(self.a)

        self.up_layout = AnchorLayout()
        self.anchor_x = "center"
        self.anchor_y = "top"
        self.up_button = Touch()
        self.up_button.bind(on_press=self.character.move_up)
        self.up_layout.add_widget(self.up_button)
        self.add_widget(self.up_layout)

        self.c = AnchorLayout()
        self.add_widget(self.c)

        self.left_layout = AnchorLayout()
        self.anchor_x = "left"
        self.anchor_y = "center"
        self.left_button = Touch()
        self.left_button.bind(on_press=self.character.move_left)
        self.left_layout.add_widget(self.left_button)
        self.add_widget(self.left_layout)

        self.e = AnchorLayout()
        self.add_widget(self.e)

        self.right_layout = AnchorLayout()
        self.anchor_x = "right"
        self.anchor_y = "center"
        self.right_button = Touch()
        self.right_button.bind(on_press=self.character.move_right)
        self.right_layout.add_widget(self.right_button)
        self.add_widget(self.right_layout)

        self.g = AnchorLayout()
        self.add_widget(self.g)

        self.down_layout = AnchorLayout()
        self.anchor_x = "center"
        self.anchor_y = "bottom"
        self.down_button = Touch()
        self.down_button.bind(on_press=self.character.move_down)
        self.down_layout.add_widget(self.down_button)
        self.add_widget(self.down_layout)

        self.i = AnchorLayout()
        self.add_widget(self.i)


class WallPaper(Image):
    def __init__(self, src="01.jpg", **kwargs):
        Image.__init__(self, **kwargs)
        self.path = "./Images/"
        self.src = src
        self.source = f"{self.path}{self.src}"
        self.keep_ratio = False
        self.size_hint = (1, 1)
        self.allow_stretch = True
        self.pos_hint = {"center_x": .5, "center_y": .5}


class Character(Image):
    def __init__(self, src="link.png", **kwargs):
        Image.__init__(self, **kwargs)
        self.path = "./Images/"
        self.src = src
        self.source = f"{self.path}{self.src}"
        self.keep_ratio = False
        self.size_hint = (.02, .02)
        self.allow_stretch = True
        self.pos_hint = {"center_x": .4, "center_y": .3}

    def move_up(self):
        self.character.pos_hint["center_y"] += 1

    def move_left(self):
        self.character.pos_hint["center_x"] -= 1

    def move_right(self):
        self.character.pos_hint["center_x"] += 1

    def move_down(self):
        self.character.pos_hint["center_y"] -= 1



