import csv
import json
from kivy.uix.gridlayout import GridLayout
from UserInterFace.StandardButton import StandardButton
from UserInterFace.StandardInput import StandardInput
from Game.User import User
from UserInterFace.StandardLabel import StandardLabel


class ConnectionView(GridLayout):
    def __init__(self, controller, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.controller = controller
        self.grid = GridLayout(cols=1)
        self.pseudo = None
        self.add_widget(self.grid)

        self.pseudo = StandardInput()
        self.grid.add_widget(self.pseudo)

        self.validation_button = StandardButton(text="Valider")
        self.validation_button.bind(on_press=self.validate)
        self.grid.add_widget(self.validation_button)

    def validate(self, widget):
        self.controller.save_user(self.pseudo.text)
        self.grid.remove_widget(self.pseudo)
        self.grid.remove_widget(self.validation_button)
        self.add_new_layout(self.pseudo.text)

    def add_new_layout(self, pseudo):
        label = StandardLabel(text=f"C'est noté, {pseudo}.")
        self.grid.add_widget(label)
        navigation_button = StandardButton(text="Jouer")
        navigation_button.bind(on_press=self.switch_to_map)
        self.grid.add_widget(navigation_button)

    def switch_to_map(self, widget):
        self.controller.display_map()

    def display_map_button(self):
        button = StandardButton(text="Map")
        button.bind(on_press=self.switch_to_map)
        self.grid.add_widget(button)


class ConnectionController():
    def __init__(self, screens, **kwargs):
        self.view = ConnectionView(self)
        self.model = ConnectionModel(self.view)
        self.screens = screens

    def display_map(self):
        self.screens.display(self.screens.map)

    def load_user(self):
        pass

    def save_user(self, pseudo):
        if pseudo != "":
            new_user = User(pseudo)
            User.add_label(new_user)
            self.model.add_user(new_user)


class ConnectionModel():
    def __init__(self, view):
        self.view = view
        self.all_users = []

    def add_user(self, new_user):
        """length_list_of_users = len(self.all_users)
        id = length_list_of_users+1"""
        pseudo = new_user.pseudo
        position = new_user.position

        self.all_users.append({"pseudo": pseudo, "position": position})
        self.write_users_datas_on_json()

    def write_users_datas_on_json(self):
        """open a file and add an Object with those datas"""
        for data in self.all_users:
            with open('users_datas.json', 'a+', encoding='utf-8') as users_datas:
                json.dump(data, users_datas, indent=4, ensure_ascii=False)

    def read_users_datas_from_json(self):
        """open a file and read an Object with those datas"""
        with open('users_datas.json', 'r') as users_datas:
            all_datas = json.load(users_datas)
        return all_datas

    def get_users(self):
        """open a file and read all Objects with those datas"""
        with open("users_datas.json", 'r') as users_datas:
            users = json.load(users_datas)
            for key, value in users.items():
                self.read_users_datas_from_json([value["Pseudo"]])

    def write_users_datas_on_json_bis(self):
        with open("users_datas.json") as users_datas:
            datas = json.load(users_datas)
            # création d'une liste d'employés
            for user_dict in datas['User']:
                for value in user_dict.values():
                    for about in value:
                        user = User(about["pseudo"], about["position"])
                        self.all_users.append(user)
