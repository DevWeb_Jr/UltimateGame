import sys
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from UserInterFace.Items import Joystick, Character
from UserInterFace.StandardButton import ReturnButton, QuitButton
from UserInterFace.Items import WallPaper


class MapView(FloatLayout):
    def __init__(self, controller, **kwargs):
        FloatLayout.__init__(self, **kwargs)
        self.controller = controller

        self.grid = GridLayout(cols=1, rows=1)
        self.add_widget(self.grid)

        self.wallpaper = WallPaper(src="map_.jpg")
        self.add_widget(self.wallpaper)

        self.character = Character()
        self.add_widget(self.character)

        self.add_layout()

        self.quit()

        self.play()

    def add_layout(self):
        button = ReturnButton(text="<<<")
        button.bind(on_press=self.switch_to_connection)
        self.wallpaper.add_widget(button)

    def switch_to_connection(self, widget):
        self.controller.display_connection()

    def quit(self):
        button = QuitButton()
        button.bind(on_press=self.close)
        self.add_widget(button)

    def close(self, widget):
        self.controller.close_app()

    def play(self):
        joystick = Joystick(self.character)
        joystick.size_hint = (None, None)
        joystick.pos = (1160, 30)
        joystick.size = (90, 90)
        self.wallpaper.add_widget(joystick)


class MapController():
    def __init__(self, screens):
        self.view = MapView(self)
        self.screens = screens

    def display_connection(self):
        self.screens.display(self.screens.connection)

    def close_app(self):
        sys.exit(0)

    def move_up(self):
        self.view.character.pos_hint["center_y"] += 1

    def move_left(self):
        self.view.character.pos_hint["center_x"] -= 1

    def move_right(self):
        self.view.character.pos_hint["center_x"] += 1

    def move_down(self):
        self.view.character.pos_hint["center_y"] -= 1
